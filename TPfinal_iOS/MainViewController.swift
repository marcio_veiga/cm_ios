//
//  MainViewController.swift
//  TPfinal_iOS
//
//  Created by Marcio vEIGA on 29/05/2017.
//  Copyright © 2017 marcio.mingos. All rights reserved.
//


import UIKit
import MapKit

class MainViewController: UIViewController {
    
    //MARK: properties
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // foco num ponto
        let initiallocation = CLLocation(latitude: 41.701497, longitude: -8.834756)
        
        centerMapOnLocation(location: initiallocation)
    
    }
    
    func centerMapOnLocation(location: CLLocation){
        //specify the rectangular region to display to get a correct zoom level too.
        
        let regionRadius: CLLocationDistance = 10000 //metros
        
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        
        //set region on map
        mapView.setRegion(coordinateRegion, animated: true)
        
        
    }
    
}
